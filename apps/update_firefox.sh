#!/bin/bash

## Mise à jour de Firefox RR sur Debian
## Conserve la version ESR issue des dépôts Debian

DIR="/opt/"
TMP="/tmp"
BIN="/usr/local/bin/firefox"
USER=$(awk -F':' -v uid=1000 '$3 == uid { print $1 }' /etc/passwd)
LOCAL_VERSION=$(sudo -u "$USER" "$BIN" -v|awk '{print $3}')
CURRENT_VERSION=$(curl -s 'https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=fr' | grep -o 'firefox-[0-9.]\+[0-9]'| cut -d- -f2)

if [ "$CURRENT_VERSION" = "$LOCAL_VERSION" ]
	then echo -e "\nLa version locale de Firefox est à jour." ; exit 0 ;
fi

wget -4 -c 'https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=fr' -O $TMP/firefox.tar.bz2
tar xfj $TMP/firefox.tar.bz2 -C $DIR
rm $TMP/firefox.tar.bz2

cat << EOF > /usr/share/applications/firefox.desktop 
[Desktop Entry]
Name=Firefox
Comment=Navigateur Web
Exec=/opt/firefox/firefox %u
Terminal=false
Type=Application
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
Actions=Private;

[Desktop Action Private]
Exec=/opt/firefox/firefox --private-window %u
Name=Navigation privée
EOF

if [ ! -e $BIN ]
	then ln -s /opt/firefox/firefox /usr/local/bin/firefox
fi

exit 0
