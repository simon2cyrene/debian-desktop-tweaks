#!/bin/bash

## Mise à jour de LibreOffice

DIR=/opt/
TMP=/tmp/libreoffice_update
LOCAL_VERSION=$(libreoffice --version | cut -d' ' -f2 | cut -d. -f1-3)
CURRENT_VERSION=$(lynx --dump https://archlinux.org/packages/extra/x86_64/libreoffice-still/ | grep libreoffice-still | awk 'NR==1{print $2}' | cut -d- -f1)
#CURRENT_VERSION="7.4.7"
LINK=/usr/local/bin/libreoffice

if [ $CURRENT_VERSION = $LOCAL_VERSION ]
	then echo -e "\nLa version locale de LibreOffice est à jour." ; exit 0 ;
	else echo -e "\nLa version locale $LOCAL_VERSION peut être mise à jour vers la version $CURRENT_VERSION." ;
fi


apt remove -y -q libreoffice*

if [ -L $LINK ]
    then rm -f $LINK ;
fi

mkdir $TMP
cd $TMP

wget -c https://download.documentfoundation.org/libreoffice/stable/$CURRENT_VERSION/deb/x86_64/LibreOffice_$CURRENT_VERSION\_Linux_x86-64_deb.tar.gz
wget -c https://download.documentfoundation.org/libreoffice/stable/$CURRENT_VERSION/deb/x86_64/LibreOffice_$CURRENT_VERSION\_Linux_x86-64_deb_langpack_fr.tar.gz
tar -xf LibreOffice_$CURRENT_VERSION\_Linux_x86-64_deb.tar.gz
tar -xf LibreOffice_$CURRENT_VERSION\_Linux_x86-64_deb_langpack_fr.tar.gz

FOLDERS=$(find $TMP/ -iname 'DEBS' -type d)

for folder in $FOLDERS
  do dpkg -i $folder/*.deb
done

rm -R $TMP

SHORT_VERSION=$(echo $CURRENT_VERSION | cut -d. -f1-2)

ln -s /usr/local/bin/libreoffice$SHORT_VERSION $LINK

DESKTOP_FILES=$(find /usr/share/applications/ -iname 'libreoffice*.desktop')

for file in $DESKTOP_FILES
  do sed -i "s/Name=LibreOffice $SHORT_VERSION/Name=LibreOffice/g" $file
done

apt install -f

exit 0
